\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnot. 
% If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings} % needed for the inclusion of source code

\usepackage{mips}
\usepackage[capitalise]{cleveref}

\makeatletter
\def\lst@makecaption{%
	\def\@captype{table}%
	\@makecaption
}
\makeatother

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\renewcommand{\ttdefault}{cmtt}
\renewcommand\lstlistingname{\textsc{Code Snippet}}
\crefname{listing}{\textsc{Code Snippet}}{\textsc{Code Snippet}}
\renewcommand\figurename{\textsc{Figure}}
\crefname{figure}{\textsc{Figure}}{\textsc{Figures}} 
\crefname{table}{\textsc{Table}}{\textsc{Tables}} 

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEGIN DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{CS274 Project: Semantic Textual Similarity using Transfer Learning and Embeddings}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHOR %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{\IEEEauthorblockN{Inhee Park}
	\IEEEauthorblockA{\textit{Department of Computer Science},
		\textit{San Jose State University}, San Jose, CA, USA}  \texttt{inhee.park@sjsu.edu} }

\maketitle
\thispagestyle{plain}
\pagestyle{plain}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ABSTRACT %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
Semantic Textual Similarity (STS) is a one of Natural Language Processing
tasks to measure closeness of contextual meaning of given words, sentences or paragraphs as the way of human understands language using a computer model. Owing to the state-of-art, general-purpose, deep learning based NLP model such as BERT, we can build a STS model utilizing Transfer Learning without training a model from scratch.  The core theme of this project is to understand word or sentence embeddings, which are features of textual data in numerical representative vectors. We use various embeddings as features to measure semantic metric such as cosine similarity and angular distance similarity. Finally, we build a semantic search engine trained on the Quora Question Set using the best embeddings together with faster and efficient `Approximate k-Nearest Neighbors' (AkNN) in lieu of brute-force cosine similarity metric.\\
GitLab repository: \texttt{https://gitlab.com/ipark/cs274-wi}
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KEY WORDS %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{IEEEkeywords}
Semantic Textual Similarity (STS), Natural Language Processing (NLP), 
Bidirectional Encoder Representations from Transformers (BERT)
Word Embeddings,  Sentence Embeddings
\end{IEEEkeywords}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INTRODUCTION %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{I. Introduction}}
\subsection*{\textbf{Importance of Sentence Textual Similarity}}
Natural Language Processing (NLP) is {\em Deep} Text Understanding distinct from {\em Shallow} Text Mining. NLP infers and predicts relations among words, whereas Text Mining is based on the frequent word counting or keyword based queries. There are multiple important tasks in NLP domain:
Automatic speech recognition, Part-of-speech tagging, Question answering
Relation prediction, Sentiment analysis, Summarization, Semantic textual similarity and etc.\cite{STS1}

Semantic Textual Similarity (STS) is measuring the meaning-wise similarity between a pair of two texts in word-level or sentence-level, ranging from 0 (independent) to 1 (equivalent). 
Related applications are detection of paraphrase or identification of duplicate. In addition, practical user cases of STS can be 1) semantic search and 2) chatboat as summarized in \cref{f:useCases}.
\begin{figure}[th!]
    \centering
    \includegraphics[width=\columnwidth]{./Figs/useCases.png}
    \caption{Use Cases : Semantic Similarity}
    \label{f:useCases}
\end{figure}

\subsection*{\textbf{Word/Sentence Embeddings as Features}}

A requirement of NLP is to convert text data into numerical values. Preferably these values are numeric vectors (called `embeddings'), so that we can apply operations (such as doc product, norm etc.) and compute distance and direction from the distributed representation of the vectors corresponding to (contextualized) text.  

The core theme of this project is to understand word or sentence embeddings, which are features of textual data in a numerical representative vectors. We use various embeddings as features to measure semantic metric such as cosine similarity and its variant.

Contextualized embedding vectors are obtained through deep learning technique.  Owing to pre-trained for encoding abundant dataset (Wikipedia and BooksCorpus), no heavy-engineering for task specific architecture is required.\cite{bert-finetune} This is a benefit of Transfer Learning, where re-use the pre-trained model and fine-tuning with a small dataset for specific downstream task.  The pre-trained NLP model itself already incorporates local synthetic features among tokens, as well as global semantic features, thus learning rate with new data is not that high, thus usually fine-tuning requires just 1-3 epochs.\cite{bert-embd2} Even further, the learned weights are extracted from the last layers of the neural network based NLP models and are pre-processed in the form of API, such as embeddings-as-service \cite{embed-service}
and pre-trained models in the TensorFlow Hub. \cite{tfhub}

Overall hierarchical view for the specific task of STS in relation to  NLP, Embeddings, Deep Learning and Transfer Learning are depicted in \cref{f:overview}.



%
\begin{figure}[th!]
\centering
\includegraphics[width=\columnwidth]{./Figs/overview.png}
\caption{Overview of the problem statement}
\label{f:overview}
\end{figure}
%


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% LITERATURE  %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{II. Literature Survey}}
%
\subsection*{\textbf{Related Works -- Extreme Features}}
When it comes to NLP, the most frequently referred feature is Term-Frequency-Inverse-Document-Frequency (TF-IDF) by counting the frequency of terms but attenuating less important terms. Although it is good at capturing important features as show-cased in the character analysis from Jane Austen's novels as shown in \cref{f:tf-idf}.\cite{tf-idf}
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/tf-idf.png}
	\caption{Identification of main characters using TF-IDF from the text data}
	\label{f:tf-idf}
\end{figure} 
However, if our focus is rather more involved task such as STS, we need embedding vectors representing word/token distribution in relation to other words/tokens. Therefore, deep learning based NLP model is required to train the text data and let them learn the representative distribution (aka embeddings).

%
On the other hand, training a Deep Learning model from scratch for a specific downstream NLP task, for example, STS in this case, is not only inefficient (requiring lots of computing resources to train over millions of parameters) but also being prone to over-fit (STS task specific dataset is relatively much smaller than general purpose Corpus level huge dataset.  The two known extreme solutions are summarized in \cref{f:knownSolutions.png}.
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/knownSolutions.png}
	\caption{Known solutions: two extreme features}
	\label{f:knownSolutions.png}
\end{figure} 

\subsection*{\textbf{State-of-Art NLP Model: BERT \& Transformer Architecture}}
The BERT (Bidirectional Encoder Representations from Transformers) is a bidirectional pre-training for language representations developed by Google and released in 2018\cite{BERT}. Unlike previous NLP models where unidirectional left-to-right language model pre-training, the BERT uses Mask Language Model (MLM) objective enables the representation to fuse the left and the right context (aka bidirectional), allowing to pre-train a deep bidirectional Transformer. That is, text data has no label, but making a word, using it as a label, and letting the model to learn toward predicting that label is a way of BERT works. 

%
\begin{figure}[th!]
	\includegraphics[width=\columnwidth]{./Figs/bertEncoder.png}
	\includegraphics[width=\columnwidth]{./Figs/bertArchitecture.png}
	\caption{The state-of-art NLP model BERT: 
		(top) detailed view of an encoder;
		(down) overall BERT architecture}
	\label{f:bertArchitecture}
\end{figure} 
%
Top figure of \cref{f:bertArchitecture} is a detailed encoder structure in BERT. The BERT is built upon the Transformer architecture (new kind of neural network, no longer needs CNN or RNN for NLP training). It uses Query, Key, and Value matrices for attention mechanism (basically per token, finding the most similar token from left and right direction), which is a signature feature of the Transformer architecture \cite{Attention}.  The BERT learns the word/token embeddings by going deeper through same 12  encoder layers as shown in the bottom figure of  
\cref{f:bertArchitecture}.

 


%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% IMPLEMENTATION  %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{III. Implementation}}
\begin{itemize}
\item Computing Platform --- Colab \cite{colab}
\item Deep Learning Framework for Embeddings Extraction --- Pytorch and Tensorflow (2.0, 1.15) \cite{bert-finetune}
\item Machine Learning Package --- Scikit Learn
\item Python 3.6
\item Pre-trained models --- Tensorflow Hub \cite{tfhub}
\item Pre-processed Embeddings for various NLP models --- Embeddings-as-service \cite{embed-service}
\item Dataset: Quora Question Pairs Dataset 
\cite{qq}
\item Approximate k-Nearest Neighbor method --  N2 Package \cite{n2}

\end{itemize}
%
\subsection*{\textbf{Proposed Solution}}
Instead of the two extreme features for the text data (primitive TF-IDF or expensive Deep Learning Training) as mentioned in the Related Work section, transfer-learning motivated reasonable/moderate features are following as summarized in \cref{f:embeddings}: static feature of Word2Vec \cite{Word2Vec} and dynamic (aka contextualized) feature of BERT or USE (Universal Sentence Encoder) Embeddings \cite{USE}.
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/embeddings.png}
	\caption{Proposed solutions: static vs. dynamic embeddings}
	\label{f:embeddings}
\end{figure} 
%

\subsection*{\textbf{Word2Vec: Baseline Model}}
\texttt{Word2Vec} is one of method in word embeddings used as core representations of text in deep learning approaches. Text is converted to numerical vectors in a reduced vocabulary dimension mapping the association among textually related words. 

Given the characteristics of STS task itself, Word2Vec would serve as a baseline model in comparison with the dynamic embeddings shown in \cref{f:bertFineTuning}.  If we used more primitive baseline such as TF-IDF, which doesn't incorporate any relative distribution with other words, hence it won't serve any meaningful baseline.
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/bertFineTuning.png}
	\caption{Best contextualized BERT embeddings}
	\label{f:bertFineTuning}
\end{figure}


\subsection*{\textbf{Dynamic Contextualized BERT Embeddings}}
Fully leveraging the Transfer Learning technique, we can use the already learned weights of the NLP layers. Fine-tuning is a technique in Deep Learning, freezing weights that already pre-trained for most layers except the a few last layers, with which train them with smaller new dataset. Unfrozen layers can be the last one layer or combination of the last layers, etc.  As shown in the \cref{f:bertFineTuning}, the best F1 score was achieved when use concatenation of the last four layers. 
\cite{FT}


Therefore, main task is to take BERT's pooled output layers (concatenation of the last 4 layers), apply a linear layer and a sigmoid activation to produce two probability values, either similar or non-similar between the two input sentences. Codes were heavily borrowed from References \cite{bert-finetune} and \cite{colab}.

And the implementations are organized in the CoLab ipython notebook as listed in \cref{f:implementation}, consisting of mainly two parts:
1) Experimenting for Word-level and Sentence-level similarity measurement;
2) Building a Semantic Search Engine.
Code is at \texttt{Code/CS274-SemanticTexualSimilarity.ipynb} in the
GitLab repository:
\texttt{https://gitlab.com/ipark/cs274-wi}.

\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/implementation.png}
	\caption{Result on the sentence-level semantic similarity}
	\label{f:implementation}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXPERIMENTS  %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{IV. Experiments}}
%
\begin{figure}[th!]
\includegraphics[width=\columnwidth]{./Figs/agenda.png}
\caption{Overall structure of experiments}
\label{f:agenda}
\end{figure}

\subsection*{\textbf{Word-level Semantic Similarity Experiment}}
First experiment is to measure the word-level semantic similarity.  For this experiment, we use the following three sentences possessing a token \textbf{``Amazon''} whether its semantic is differentiated when it is used referring to an online retail store or to forest zone in Brazil.
\begin{enumerate}
\item \emph{\textbf{Amazon} kicks-off a full weekend of Cyber Monday deals on November 30, with new deals all day, every day and deeper discounts than ever before, including on Cyber Monday.}
\item \emph{Alibaba's Singles Day beat \textbf{Amazon}'s estimated Prime Day sales within an hour.} 
\item \emph{Brazil's role in deforestation of the \textbf{Amazon} rainforest has been a significant issue since the 1970s.}
\end{enumerate}

\subsection*{\textbf{Sentence-level Semantic Similarity Experiment}}
Second experiment is to  measure sentence-level semantic similarity. 
Examples of deep STS understanding are: 1) prediction on sentence pair for the two sentences having high semantic similarity and low lexical match; they are paraphrase with each other. For example:
\begin{itemize}
	\item \emph{Bird is washing itself in the water basin.}
	\item \emph{The bird is bathing in the sink.}
\end{itemize}
2) prediction on two sentences having low semantic similarity but high lexical match; they are not paraphrase with each other. For example:
\begin{itemize}
	\item \emph{The young lady enjoys listening to the guitar.}
	\item \emph{The young lady enjoys playing the guitar.}
\end{itemize}
 In this sense, shallow text mining can't extract such a subtle relation, only through deep leaning based NLP may be possible to estimate difference/similarity automatically.  Hence, contextualized embeddings such as BERT, USE will be used. 

\subsection*{\textbf{Semantic Similarity Metrics}}
Due to high dimensional embedding vectors (126 tokens per input sentence;
512 or 728 vector length), cosine similarity is appropriate metric where dot product of vectors are efficient to compute.  Also, little variant of angular distance similarity is sometimes well performed especially dealing with text data. Those metrics are summarized in \cref{f:simMetrics}.

%
\begin{figure}[th!]
\centering
\includegraphics[width=\columnwidth]{./Figs/simMetrics.png}
\caption{Similarity metrics}
\label{f:simMetrics}
\end{figure} 
%
\subsection*{\textbf{Semantic Search Engine}}
Underlying idea of building a semantic search engine is to eventually build a chatbot fully utilizing STS combined with an efficient similarity computation algorithm of ``Approximate kNN'' (aka AkNN) instead of brute-force, exhaustive, sentence pair-wise cosine similarity computation.  AkNN builds index hash-tree construction to group the large size of high-dimensional vectors, then search for the similar group rather than entire dataset. 
\begin{enumerate}
\item (pre-processing) For the Quora Question Pair dataset, convert them to corresponding embedding vectors using USE embedding method.
\includegraphics[width=\columnwidth]{./Figs/engine-step1.png}
\item  (pre-processing)  Construct AkNN index based on the pre-generated embedding database in step 1.
\includegraphics[width=\columnwidth]{./Figs/engine-step2.png}
\includegraphics[width=\columnwidth]{./Figs/engine-step2b.png}
\item (real-time) Upon getting new query sentence from user, convert it to corresponding embedding vector using the same USE embedding method.
\item (real-time) Search the k-index from the AkNN model in step 2 using the embedding obtained in step 3.  It returns k-index list which correspond to the similar sentences against the new user query sentence. 
\end{enumerate}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RESULSTS   %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{V. Results}}
\subsection*{\textbf{Semantic ``Word'' Similarity}}
%
\begin{figure}[th!]
\centering
\includegraphics[width=\columnwidth]{./Figs/wordSemantics.png}
\caption{Result on the word-level semantic similarity}
\label{f:wordSemantics}
\end{figure} 
\cref{f:wordSemantics} demonstrated word-level semantic similarity using a feature static Word2Vec feature vs. dynamic BERT embedding feature on the same three sentences including ``Amazon'' token.
Regardless of cosine similarity or its variant, as expected Word2Vec couldn't distinguish word semantic, resulting in identical similarity.
In contrast, also as expected, BERT embeddings could differentiate different semantic ``Amazon'' in contextualized with online store or in contextualized with forest.  Cosine similarity separates semantic similarity in these two Amazon semantics better than  the angular distance similarity.
%
\subsection*{\textbf{Semantic ``Sentence'' Similarity}}
%
\begin{figure}[th!]
\centering
\includegraphics[width=\columnwidth]{./Figs/sentenceSemantics.png}
\caption{Result on the sentence-level semantic similarity}
\label{f:sentenceSemantics}
\end{figure} 
%
However, sentence level semantics predicts opposite to the expectation.  Expectation was to embeddings can capture paraphrase relation with higher similarity score in spite of low lexical pair of sentences (highlighted with light yellow color).  But, actual similarity outcome is the higher the similarity score for more lexical matches.  Such an opposite prediction pattern is consistent regardless of embeddings and similarity metrics. 

\subsection*{\textbf{Semantic Search Engine}}
A semantic search with the following new user query question with k=3 (for AkNN),
\begin{itemize}
	\item
\emph{What is the most interesting computer science field?}
\end{itemize}
the Semantic Quora Search Engine suggested the following three the most similar questions (also screen capture in \cref{f:engine})
\begin{enumerate}
\item	\emph{What is the best field of engineering?}
\item	\emph{Which are the best engineering fields?}
\item	\emph{What computer science courses should I take If I want to join a product based or e-commerce company?}
\end{enumerate}

\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/engine2.png}
	\includegraphics[width=\columnwidth]{./Figs/engine3.png}
	\caption{Result on the Semantic Quora Question Search}
	\label{f:engine}
\end{figure} 
Though there is no ground truth to verify the quality of this semantic engine.  But based on the human perception, resulting three questions are quite semantically similar to the query question.
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CONCLUSION   %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{VI. Discussion \& Conclusion}} 
Using the transfer learning technique in deep learning, 
we could obtain the word/sentence embedding vectors
from the already trained general purpose NLP models without extra
fine-tuning.  Both Tensorflow Hub and embeddings-as-service provides
the tensors/vectors/embeddings from the last layers of the pre-trained modes, which can be used as features to measure cosine similarity of
the input sentence of interest.

Yet, in spite of advanced NLP models, as experimented in this project, lexically similar sentences show higher similarity than semantically similar sentences.  We note that other NLP researchers also had similar problems; as a solution, they constructed enhanced Quora Question dataset by scrambling the word inside original dataset (called `Paraphrase Adversaries from Word Scrambling') and re-train the BERT model with PAWS dataset.  And they demonstrated more correct STS results.
%
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/PAWSset.png}
	\includegraphics[width=\columnwidth]{./Figs/PAWSbert.png}
	\caption{(top) Paraphrase Adversaries from Word Scrambling; (bottom) improved STS results. Reference: \cite{PAWS}}
	\label{f:PAWS}
\end{figure} 
%
We found that identifying the pair of sentences with low lexical match as paraphrase is a known issue regarding the Quora Question Pair dataset.  To improve prediction for such challenging cases, researchers construct an enhanced dataset, called Paraphrase Adversaries from Word Scrambling (PAWS) by scrambling words to generate more dataset based on the Quora dataset.  Then as shown in the \cref{f:PAWS}, train this enhanced PAWS dataset using the same base architecture of BERT, those challenging paraphrase sentence pairs were correctly predicted (see columns Gold as Ground Truth vs. BERT+PAWS).

This project provides invaluable opportunities to explore state-of-art NLP models (BERT), transfer-learning techniques using embeddings from various pre-trained models, potential application of semantic textual similarity, and efficient faster similarity search method of approximate k-nearest neighbor.

Future work may use Amazon customer Question-Answer pair dataset to build real ChatBot (Question comes, Answer provided by measuring semantic similarity of query sentence against database embedding vectors).
 

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BIBLOIGRAPHY %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{00}
\bibitem{STS1}
http://nlpprogress.com/english/semantic\_textual\_similarity.html

\bibitem{tf-idf}
TERM FREQUENCY AND TF-IDF USING TIDY DATA PRINCIPLES https://juliasilge.com/blog/term-frequency-tf-idf/Important words!

\bibitem{bert-finetune}
https://mccormickml.com/2019/07/22/BERT-fine-tuning/

\bibitem{bert-embd2}
https://mccormickml.com/2019/05/14/BERT-word-embeddings-tutorial/

\bibitem{embed-service}
https://pypi.org/project/embedding-as-service/

\bibitem{tfhub}
https://tfhub.dev/s?dataset=wikipedia-and-bookscorpus

\bibitem{BERT}
BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding \texttt{https://arxiv.org/abs/1810.04805}

\bibitem{Attention}
Attention Is All You Need - https://arxiv.org/abs/1706.03762

\bibitem{colab}
https://colab.research.google.com/github/tensorflow/tpu/blob/master/tools\\/colab/bert\_finetuning\_with\_cloud\_tpus.ipynb

\bibitem{qq}
https://www.kaggle.com/c/quora-question-pairs

\bibitem{n2}
https://github.com/kakao/n2
%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibitem{Word2Vec}
Distributed Representations of Words and Phrases and their Compositionality - https://arxiv.org/abs/1310.4546

\bibitem{USE}
Universal Sentence Encoder - https://arxiv.org/abs/1803.11175


\bibitem{FT}
How to Fine-Tune BERT for Text Classification? - https://arxiv.org/abs/1905.05583


\bibitem{PAWS}
Paraphrase Adversaries from Word Scrambling- https://arxiv.org/abs/1904.01130



	


\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
