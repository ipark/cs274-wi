# %%
'''
# CS274: Semantic Textual Similarity & Semantic Quora Question Search
'''

# %%
'''
## Word-level Semantic Similarity by BERT Fine-Tuninig
'''

# %%
!pip install pytorch-pretrained-bert

# %%
import torch
from pytorch_pretrained_bert import BertTokenizer, BertModel, BertForMaskedLM

# OPTIONAL: if you want to have more information on what's happening, activate the logger as follows
import logging
#logging.basicConfig(level=logging.INFO)

import matplotlib.pyplot as plt
% matplotlib inline

# Load pre-trained model tokenizer (vocabulary)
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')


# %%
text = "Amazon kicks-off a full weekend of Cyber Monday deals on November 30, with new deals all day, every day and deeper discounts than ever before, including on Cyber Monday.; Alibaba's Singles Day beat Amazon's estimated Prime Day sales within an hour ; Brazil's role in deforestation of the Amazon rainforest has been a significant issue since the 1970s"
marked_text = "[CLS] " + text + " [SEP]"

print (marked_text)

# %%
tokenized_text = tokenizer.tokenize(marked_text)
print (tokenized_text)

# %%
indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)

for tup in zip(tokenized_text, indexed_tokens):
  print (tup)

# %%
segments_ids = [1] * len(tokenized_text)
print (segments_ids)

# %%
# Convert inputs to PyTorch tensors
tokens_tensor = torch.tensor([indexed_tokens])
segments_tensors = torch.tensor([segments_ids])

# Load pre-trained model (weights)
model = BertModel.from_pretrained('bert-base-uncased')

# Put the model in "evaluation" mode, meaning feed-forward operation.
model.eval()

# %%
# Predict hidden states features for each layer
with torch.no_grad():
    encoded_layers, _ = model(tokens_tensor, segments_tensors)

# %%
layer_i = 0
batch_i = 0
token_i = 0


# %%
# Convert the hidden state embeddings into single token vectors

# Holds the list of 12 layer embeddings for each token
# Will have the shape: [# tokens, # layers, # features]
token_embeddings = [] 

# For each token in the sentence...
for token_i in range(len(tokenized_text)):
  
  # Holds 12 layers of hidden states for each token 
  hidden_layers = [] 
  
  # For each of the 12 layers...
  for layer_i in range(len(encoded_layers)):
    
    # Lookup the vector for `token_i` in `layer_i`
    vec = encoded_layers[layer_i][batch_i][token_i]
    
    hidden_layers.append(vec)
    
  token_embeddings.append(hidden_layers)


# %%
# Stores the token vectors, with shape [22 x 768]
token_vecs_sum = []

# For each token in the sentence...
for token in token_embeddings:
    # Sum the vectors from the last four layers.
    sum_vec = torch.sum(torch.stack(token)[-4:], 0)
    
    # Use `sum_vec` to represent `token`.
    token_vecs_sum.append(sum_vec)

# %%
import numpy as np
from numpy import pi
def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist

# %%
for i,x in enumerate(tokenized_text):
  if x =='Amazon':
    print (i,x)
  elif x =='amazon':
    print (i,x)

# %%
from sklearn.metrics.pairwise import cosine_similarity

a1=token_vecs_sum[1].reshape(1,-1)
a2=token_vecs_sum[46].reshape(1,-1)
a3=token_vecs_sum[66].reshape(1,-1)
print('[Amazon] cyber Monday; [Amazon] vs Alibaba', cosine_similarity(a1, a2))
print('[Amazon] cyber Monday; [Amazon] in Brazil', cosine_similarity(a1, a3))
print('[Amazon] vs Alibaba; [Amazon] in Brazil', cosine_similarity(a2, a3))

# %%

print('[Amazon] cyber Monday; [Amazon] vs Alibaba', angular_distance(cosine_similarity(a1, a2)))
print('[Amazon] cyber Monday; [Amazon] in Brazil', angular_distance(cosine_similarity(a1, a3)))
print('[Amazon] vs Alibaba; [Amazon] in Brazil', angular_distance(cosine_similarity(a2, a3)))

# %%
'''
## Semantic Similarity using Word Embeddings from TF Hub (NNLM)
'''

# %%
import tensorflow as tf
import tensorflow_hub as hub
import numpy as np

# Import the Universal Sentence Encoder's TF Hub module

embed = hub.load("https://tfhub.dev/google/tf2-preview/nnlm-en-dim128/1")

def get_features(texts):
    if type(texts) is str:
        texts = [texts]
    with tf.Session() as sess:
        sess.run([tf.global_variables_initializer(), tf.tables_initializer()])
        return sess.run(embed(texts))

# %%
import numpy as np
from numpy import pi

def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist

def cosine_similarity(v1, v2):
    mag1 = np.linalg.norm(v1)
    mag2 = np.linalg.norm(v2)
    return np.dot(v1, v2) / (mag1 * mag2)

text1a="Bird is washing itself in the water basin."
text1b="The bird is bathing in the sink."
text2a="The young lady enjoys listening to the guitar."
text2b="The young lady enjoys playing the guitar."
vec1 = get_features(text1a)[0]
vec2 = get_features(text1b)[0]
vec3 = get_features(text2a)[0]
vec4 = get_features(text2b)[0]
print("CosSim(high semantic, low lexical)=%.2f" % cosine_similarity(vec1, vec2))
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosine_similarity(vec1, vec2)))
print("CosSim(low semantic, high lexical)=%.2f" % cosine_similarity(vec3, vec4))
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosine_similarity(vec3, vec4)))

# %%
'''
## Semantic Similarity using Word Embeddings from TF Hub (Universal Sentence Embedding)
'''

# %%

from absl import logging
import tensorflow as tf
import tensorflow_hub as hub
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import re
import seaborn as sns

# Import the Universal Sentence Encoder's TF Hub module
embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-large/3")

def get_features(texts):
    if type(texts) is str:
        texts = [texts]
    with tf.Session() as sess:
        sess.run([tf.global_variables_initializer(), tf.tables_initializer()])
        return sess.run(embed(texts))

# %%
import numpy as np
from numpy import pi

def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist

def cosine_similarity(v1, v2):
    mag1 = np.linalg.norm(v1)
    mag2 = np.linalg.norm(v2)
    return np.dot(v1, v2) / (mag1 * mag2)

text1a="Bird is washing itself in the water basin."
text1b="The bird is bathing in the sink."
text2a="The young lady enjoys listening to the guitar."
text2b="The young lady enjoys playing the guitar."
vec1 = get_features(text1a)[0]
vec2 = get_features(text1b)[0]
vec3 = get_features(text2a)[0]
vec4 = get_features(text2b)[0]
print("CosSim(high semantic, low lexical)=%.2f" % cosine_similarity(vec1, vec2))
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosine_similarity(vec1, vec2)))
print("CosSim(low semantic, high lexical)=%.2f" % cosine_similarity(vec3, vec4))
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosine_similarity(vec3, vec4)))

# %%
'''
## Semantic Similarity using Word2Vec (context-free Embeddings)
'''

# %%
! pip install embedding-as-service

# %%
from embedding_as_service.text.encode import Encoder  
from sklearn.metrics.pairwise import cosine_similarity

betterTokenizer = Encoder(embedding='bert', model='bert_base_uncased', download=False)
text = "Amazon kicks-off a full weekend of Cyber Monday deals on November 30, with new deals all day, every day and deeper discounts than ever before, including on Cyber Monday.; Alibaba's Singles Day beat Amazon's estimated Prime Day sales within an hour ; Brazil's role in deforestation of the Amazon rainforest has been a significant issue since the 1970s"
tokens = betterTokenizer.tokenize(texts=[text]) 
for i, t in enumerate(tokens[0]):
  if t == "amazon" or t == "Amazon":
    print(1+i, t)

# %%
embed = Encoder(embedding='word2vec', model='google_news_300', download=True) 
vecs = embed.encode(tokens, is_tokenized=True)
vecs = np.squeeze(vecs) 

# %%
from sklearn.metrics.pairwise import cosine_similarity
a1=vecs[0].reshape(1,-1)
a2=vecs[45].reshape(1,-1)
a3=vecs[65].reshape(1,-1)
print('Cosim:[Amazon] cyber Monday; [Amazon] vs Alibaba', cosine_similarity(a1, a2)[0][0])
print('Cosim:[Amazon] cyber Monday; [Amazon] in Brazil', cosine_similarity(a1, a3)[0][0])
print('Cosim:[Amazon] vs Alibaba; [Amazon] in Brazil', cosine_similarity(a2, a3)[0][0])

# %%
import numpy as np
from numpy import pi
def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist
print('AngSim: [Amazon] cyber Monday; [Amazon] vs Alibaba', angular_distance(cosine_similarity(a1, a2))[0][0])
print('AngSim: [Amazon] cyber Monday; [Amazon] in Brazil', angular_distance(cosine_similarity(a1, a3))[0][0])
print('AngSim: [Amazon] vs Alibaba; [Amazon] in Brazil', angular_distance(cosine_similarity(a2, a3))[0][0])

# %%
import numpy as np
from numpy import pi
def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist
text1a="Bird is washing itself in the water basin."
text1b="The bird is bathing in the sink."
text2a="The young lady enjoys listening to the guitar."
text2b="The young lady enjoys playing the guitar."
vec1=embed.encode([text1a])
vec2=embed.encode([text1b])
vec3=embed.encode([text2a])
vec4=embed.encode([text2b])
(n,x,y)=vec1.shape; v1=vec1.reshape((n, x*y))
(n,x,y)=vec2.shape; v2=vec2.reshape((n, x*y))
(n,x,y)=vec3.shape; v3=vec3.reshape((n, x*y))
(n,x,y)=vec4.shape; v4=vec4.reshape((n, x*y))
cosSim12=cosine_similarity(v1, v2)
cosSim34=cosine_similarity(v3, v4)
print("CosSim(high semantic, low lexical)=%.2f" % cosSim12[0][0])
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosSim12)[0][0])
print("CosSim(low semantic, high lexical)=%.2f" % cosSim34[0][0])
print("AngDist(low semantic, high lexical)=%.2f" % angular_distance(cosSim34)[0][0])


# %%
'''
## Semantic Similarity using BERT-base-uncased (Contextualized Embeddings)
'''

# %%
import numpy as np
from numpy import pi
from embedding_as_service.text.encode import Encoder  
from sklearn.metrics.pairwise import cosine_similarity

def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist
embed = Encoder(embedding='bert', model='bert_base_uncased', download=True)

# %%
text = "Amazon kicks-off a full weekend of Cyber Monday deals on November 30, with new deals all day, every day and deeper discounts than ever before, including on Cyber Monday.; Alibaba's Singles Day beat Amazon's estimated Prime Day sales within an hour ; Brazil's role in deforestation of the Amazon rainforest has been a significant issue since the 1970s"
tokens = embed.tokenize(texts=[text]) 
for i, t in enumerate(tokens[0]):
  if t == "amazon" or t == "Amazon":
    print(1+i, t)


# %%
vecs = embed.encode([text])  
print(vecs.shape)
vecs = np.squeeze(vecs)

# %%
from sklearn.metrics.pairwise import cosine_similarity
a1=vecs[1].reshape(1,-1)
a2=vecs[46].reshape(1,-1)
a3=vecs[66].reshape(1,-1)
print('Cosim:[Amazon] cyber Monday; [Amazon] vs Alibaba', cosine_similarity(a1, a2)[0][0])
print('Cosim:[Amazon] cyber Monday; [Amazon] in Brazil', cosine_similarity(a1, a3)[0][0])
print('Cosim:[Amazon] vs Alibaba; [Amazon] in Brazil', cosine_similarity(a2, a3)[0][0])

# %%
import numpy as np
from numpy import pi
def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist
print('AngSim: [Amazon] cyber Monday; [Amazon] vs Alibaba', angular_distance(cosine_similarity(a1, a2))[0][0])
print('AngSim: [Amazon] cyber Monday; [Amazon] in Brazil', angular_distance(cosine_similarity(a1, a3))[0][0])
print('AngSim: [Amazon] vs Alibaba; [Amazon] in Brazil', angular_distance(cosine_similarity(a2, a3))[0][0])

# %%
# Paraphrases
text1a="Bird is washing itself in the water basin."
text1b="The bird is bathing in the sink."
# NOT paraphrases
text2a="The young lady enjoys listening to the guitar."
text2b="The young lady enjoys playing the guitar."

vec1=embed.encode([text1a])
vec2=embed.encode([text1b])
vec3=embed.encode([text2a])
vec4=embed.encode([text2b])
(n,x,y)=vec1.shape; v1=vec1.reshape((n, x*y))
(n,x,y)=vec2.shape; v2=vec2.reshape((n, x*y))
(n,x,y)=vec3.shape; v3=vec3.reshape((n, x*y))
(n,x,y)=vec4.shape; v4=vec4.reshape((n, x*y))
print(v1)
cosSim12=cosine_similarity(v1, v2)
cosSim34=cosine_similarity(v3, v4)
print("CosSim(high semantic, low lexical)=%.2f" % cosSim12[0][0])
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosSim12)[0][0])
print("CosSim(low semantic, high lexical)=%.2f" % cosSim34[0][0])
print("AngDist(low semantic, high lexical)=%.2f" % angular_distance(cosSim34)[0][0])


# %%
#Paraphrase: 
text1a="Symptoms of influenza include fever and nasal congestion."
text1b="A stuffy nose and elevated temperature are signs you may have the flu."
#Paraphrase: 
text2a="The price of a resort vacation typically includes meals, tips and equipment rentals, which makes your trip more cost-effective."
text2b="All-inclusive resort vacations can make for an economical trip."

# %%
vec1=embed.encode([text1a])
vec2=embed.encode([text1b])
vec3=embed.encode([text2a])
vec4=embed.encode([text2b])
(n,x,y)=vec1.shape; v1=vec1.reshape((n, x*y))
(n,x,y)=vec2.shape; v2=vec2.reshape((n, x*y))
(n,x,y)=vec3.shape; v3=vec3.reshape((n, x*y))
(n,x,y)=vec4.shape; v4=vec4.reshape((n, x*y))
cosSim12=cosine_similarity(v1, v2)
cosSim34=cosine_similarity(v3, v4)
print("\n----similarity---")
print(text1a)
print(text1b)
print(" =>CosSim(1a,1b)=%.2f" % cosSim12[0][0])
print(" =>AngDist(1a,1b)=%.2f" % angular_distance(cosSim12)[0][0])
print("\n----similarity---")
print(text2a)
print(text2b)
print(" =>CosSim(2a,2b)=%.2f" % cosSim34[0][0])
print(" =>AngDist(2a,2b)=%.2f" % angular_distance(cosSim34)[0][0])


# %%
'''
## Semantic Similarity using BERT-large-uncased
'''

# %%
import numpy as np
from numpy import pi
def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist
text1a="Bird is washing itself in the water basin."
text1b="The bird is bathing in the sink."
text2a="The young lady enjoys listening to the guitar."
text2b="The young lady enjoys playing the guitar."
embed = Encoder(embedding='bert', model='bert_large_uncased', download=True)
vec1=embed.encode([text1a])
vec2=embed.encode([text1b])
vec3=embed.encode([text2a])
vec4=embed.encode([text2b])
(n,x,y)=vec1.shape; v1=vec1.reshape((n, x*y))
(n,x,y)=vec2.shape; v2=vec2.reshape((n, x*y))
(n,x,y)=vec3.shape; v3=vec3.reshape((n, x*y))
(n,x,y)=vec4.shape; v4=vec4.reshape((n, x*y))
cosSim12=cosine_similarity(v1, v2)
cosSim34=cosine_similarity(v3, v4)
print("CosSim(high semantic, low lexical)=%.2f" % cosSim12[0][0])
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosSim12)[0][0])
print("CosSim(low semantic, high lexical)=%.2f" % cosSim34[0][0])
print("AngDist(low semantic, high lexical)=%.2f" % angular_distance(cosSim34)[0][0])


# %%
from embedding_as_service.text.encode import Encoder  
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from numpy import pi
def angular_distance(cos_sim):
  # 1-arccos[(<X, Y>/(||X||*||Y||)/pi]
  dist = 1 - np.arccos(cos_sim)/pi
  return dist
text1a="Bird is washing itself in the water basin."
text1b="The bird is bathing in the sink."
text2a="The young lady enjoys listening to the guitar."
text2b="The young lady enjoys playing the guitar."
embed = Encoder(embedding='bert', model='bert_large_uncased', download=True)
vec1=embed.encode([text1a])
vec2=embed.encode([text1b])
vec3=embed.encode([text2a])
vec4=embed.encode([text2b])
(n,x,y)=vec1.shape; v1=vec1.reshape((n, x*y))
(n,x,y)=vec2.shape; v2=vec2.reshape((n, x*y))
(n,x,y)=vec3.shape; v3=vec3.reshape((n, x*y))
(n,x,y)=vec4.shape; v4=vec4.reshape((n, x*y))
cosSim12=cosine_similarity(v1, v2)
cosSim34=cosine_similarity(v3, v4)
print("CosSim(high semantic, low lexical)=%.2f" % cosSim12[0][0])
print("AngDist(high semantic, low lexical)=%.2f" % angular_distance(cosSim12)[0][0])
print("CosSim(low semantic, high lexical)=%.2f" % cosSim34[0][0])
print("AngDist(low semantic, high lexical)=%.2f" % angular_distance(cosSim34)[0][0])


# %%
'''
# Semantic Quora Question Search
'''

# %%
'''
### 1. All Quora Question DataSet in Universal Sentence Encoder Embeddings
'''

# %%
import pandas as pd
df = pd.read_pickle("/content/USEembedQQ.pkl")
df.shape
df.columns

# %%
embeddings = df['embeddings']

# %%
print(embeddings.shape)
len(embeddings[1993])

# %%
'''
### 2. Build Index of Embeddings using Approximate Nearest Neighbor (N2 package) 
'''

# %%
!pip install n2

# %%
# Approximate Nearest Neighbor (N2 package)
import numpy as np
from n2 import HnswIndex
embedDim = len(embeddings[0])
index=HnswIndex(embedDim)
for embed in embeddings:
  index.add_data(embed)
index.build(m=3, n_threads=4)


# %%
'''
### 3. Functions to get Embedding from Query Question
'''

# %%
import tensorflow as tf
import tensorflow_hub as hub

module_url = "https://tfhub.dev/google/universal-sentence-encoder-large/3"
embed = hub.Module(module_url)

def generate_embedding(messages):
    with tf.Session() as session:
      session.run([tf.global_variables_initializer(), tf.tables_initializer()])
      message_embeddings = session.run(embed(messages))
    return(message_embeddings[0])

# %%
new_emb = generate_embedding(["What is the most interesting computer science field?"])

# %%
def find_similar_items(embedding, k):
  '''Finds similar items to a given embedding in the ANN index'''
  ids = index.search_by_vector(embedding, k, include_distances=True)
  items = [df['questions'][i[0]] for i in ids]
  return items

# %%
k=3
find_similar_items(new_emb, k)
